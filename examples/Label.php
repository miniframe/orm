<?php

namespace App\Model;

use Miniframe\ORM\Annotation\Column;
use Miniframe\ORM\Annotation\Table;

/**
 * It's also possible to specify a table name that's not equal to the entity name.
 *
 * @Table(name="labels")
 */
class Label
{
    /**
     * Column with auto increment ID.
     *
     * You can specify multiple properties: primaryKey, autoIncrement and unsigned are used here.
     * The name is used from the variable name ($id)
     * The column type is used from the 'var' annotation.
     *
     * @Column(primaryKey, autoIncrement, unsigned)
     *
     * @var int
     */
    protected $id;

    /**
     * Simple text column, string will be converted to varchar(255) unless otherwise specified.
     *
     * @Column
     * @var    string
     */
    protected $name;

    /**
     * Reference to the Example ID
     *
     * @Column(name="example_id", unsigned)
     *
     * @var int
     */
    protected $example;

    /**
     * Setter method for the 'name' column.
     *
     * @param string $name The name.
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Setter method for the 'example_id' column.
     *
     * @param integer $exampleId The ID.
     *
     * @return void
     */
    public function setExampleId(int $exampleId): void
    {
        $this->example = $exampleId;
    }

    /**
     * Getter method for the 'id' column.
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Getter method for the 'name' column.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Getter method for the 'example_id' column.
     *
     * @return integer
     */
    public function getExampleId(): int
    {
        return $this->example;
    }
}

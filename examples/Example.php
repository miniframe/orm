<?php

namespace App\Model;

use Miniframe\ORM\Annotation\Column;
use Miniframe\ORM\Annotation\OneToMany;
use Miniframe\ORM\Annotation\Table;

/**
 * This entity is an example of what is possible with this ORM.
 *
 * Columns can be private, protected or public.
 *
 * Get and Set methods are not used by the ORM but useful for your own application.
 * This way it's easier to, for example, enforce a column to be read only.
 *
 * @Table(name="example")
 */
class Example
{
    /**
     * Column with auto increment ID.
     *
     * You can specify multiple properties: primaryKey, autoIncrement and unsigned are used here.
     * The name is used from the variable name ($id)
     * The column type is used from the 'var' annotation.
     *
     * @Column(primaryKey, autoIncrement, unsigned)
     *
     * @var int
     */
    protected $id;

    /**
     * A DateTime column is translated to a DateTime object.
     *
     * @Column
     *
     * @var \DateTime
     */
    protected $creationDate;

    /**
     * Simple text column, string will be converted to varchar(255) unless otherwise specified.
     *
     * @Column
     * @var    string
     */
    protected $name;

    /**
     * This column won't be a varchar(255) but will have type 'text'.
     * Also, the column name in the database will be 'full_address'.
     *
     * @Column(name="full_address", type="text")
     *
     * @var string
     */
    protected $address;

    /**
     * The 'tag' column can be null, so the property nullable is given.
     * Keep in mind, multiple variables are used in the 'var' annotation.
     * The first one will be used to determine the column type in the database.
     *
     * @Column(nullable)
     *
     * @var string|null
     */
    protected $tag = null;

    /**
     * This column will be renamed in the database from camelCase to snake_case.
     *
     * @Column
     * @var    string
     */
    protected $camelCasedText;

    /**
     * This is not a column but a reference to another table.
     *
     * The entity that is linked is the class 'Label' in the same namespace as this entity.
     * The property that's defined in the remote entity that refers to this one is the 'example' property.
     * The property value that's in the remote table should match the value of the localJoinProperty column: 'id'.
     *
     * The 'remoteJoinProperty' and 'localJoinProperty' are not required in this case since they default to the name of
     * the current entity (which is 'Example') and the primary key property of the current entity (which is 'id'),
     * but for educational purposes I wrote them down anyway.
     *
     * @OneToMany(remoteEntityClass="Label", remoteJoinProperty="example", localJoinProperty="id")
     *
     * @var Label[]
     */
    protected $labels;

    /**
     * Triggered when a new instance is created.
     */
    public function __construct()
    {
        // The default value can be specified in some database engines (CURRENT_TIMESTAMP in mysql for example),
        // but this works for all engines.
        if ($this->creationDate === null) {
            $this->creationDate = new \DateTime();
        }
    }

    /**
     * Getter method for the 'id' column.
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Getter method for the 'creation_date' column.
     *
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * Getter method for the 'name' column.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Getter method for the 'address' column.
     *
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * Getter method for the 'tag' column.
     *
     * @return string|null
     */
    public function getTag(): ?string
    {
        return $this->tag;
    }

    /**
     * Getter method for the 'camel_cased_text' column.
     *
     * @return string
     */
    public function getCamelCasedText(): string
    {
        return $this->camelCasedText;
    }

    /**
     * Returns all 'one to many' joined labels.
     *
     * @return Label[]
     */
    public function getLabels(): iterable
    {
        return $this->labels;
    }

    /**
     * Setter method for the 'name' column.
     *
     * @param string $name The name.
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Setter method for the 'address' column.
     *
     * @param string $address The address.
     *
     * @return void
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * Setter method for the 'tag' column.
     *
     * @param string|null $tag The tag(s).
     *
     * @return void
     */
    public function setTag(?string $tag): void
    {
        $this->tag = $tag;
    }

    /**
     * Setter method for the 'camel_cased_text' column.
     *
     * @param string $text The new text value.
     *
     * @return void
     */
    public function setCamelCasedText(string $text): void
    {
        $this->camelCasedText = $text;
    }
}

<?php

namespace Miniframe\ORM\Engine;

use Garrcomm\PHPUnitHelpers\Mysqli\MysqliMock;
use Miniframe\Core\Config;
use PHPUnit\Framework\TestCase;

class MySQLTest extends TestCase
{
    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        MysqliMock::reset();
        parent::tearDown();
    }

    /**
     * This method is called before the first test of this test class is run.
     *
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        MysqliMock::init();
        parent::setUpBeforeClass();
    }

    /**
     * Called in multiple tests.
     *
     * @param array $additionalConfig Additional config values.
     *
     * @return MySQL
     */
    public static function getEngine(array $additionalConfig = array()): MySQL
    {
        $configData = array_merge([
            'orm' => [
                'engine' => 'MySQL',
                'mysql_hostname' => 'foo.bar',
                'mysql_username' => 'foo',
                'mysql_password' => 'bar',
                'mysql_database' => 'foobar',
                'mysql_port' => 1337,
            ],
        ], $additionalConfig);

        $config = Config::__set_state([
            'data' => $configData,
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
        ]);
        $engine = new MySQL($config);

        return $engine;
    }

    /**
     * Tests the constructor, which should result in a mysql connection
     *
     * @return void
     */
    public function testConstructor(): void
    {
        $engine = static::getEngine();

        // Did the \mysqli constructor get all proper values?
        MysqliMock::assertConstructor(function (
            ?string $hostname,
            ?string $username,
            ?string $password,
            ?string $database,
            ?int $port,
            ?string $socket
        ) {
            $this->assertEquals('foo.bar', $hostname);
            $this->assertEquals('foo', $username);
            $this->assertEquals('bar', $password);
            $this->assertEquals('foobar', $database);
            $this->assertEquals(1337, $port);
            $this->assertNull($socket);
        });

        // Are the proper flags set with mysqli_report?
        MysqliMock::assertMysqliReport(function (int $flags) {
            // Test if we have MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT (which evaluates to (int)3)
            $this->assertEquals(3, $flags);
        });
    }

    /**
     * Tests the socket value
     *
     * @return void
     */
    public function testConstructorWithSocket(): void
    {
        $engine = static::getEngine([
            'orm' => [
                'mysql_hostname' => null,
                'mysql_username' => 'foo',
                'mysql_password' => 'bar',
                'mysql_port' => null,
                'mysql_socket' => __FILE__,
            ]
        ]);

        // Did the \mysqli constructor get all proper values?
        MysqliMock::assertConstructor(function (
            ?string $hostname,
            ?string $username,
            ?string $password,
            ?string $database,
            ?int $port,
            ?string $socket
        ) {
            $this->assertNull($hostname);
            $this->assertEquals('foo', $username);
            $this->assertEquals('bar', $password);
            $this->assertNull($database);
            $this->assertNull($port);
            $this->assertEquals(__FILE__, $socket);
        });
    }

    /**
     * Tests the getConnection() call
     *
     * @return void
     */
    public function testGetConnection(): void
    {
        $this->assertInstanceOf(\mysqli::class, static::getEngine()->getConnection());
    }
}

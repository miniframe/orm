<?php

namespace Miniframe\ORM\Middleware;

use Garrcomm\PHPUnitHelpers\Mysqli\MysqliMock;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\ORM\Engine\EngineInterface;
use PHPUnit\Framework\TestCase;

class ORMTest extends TestCase
{
    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        MysqliMock::reset();
        parent::tearDown();
    }

    /**
     * This method is called before the first test of this test class is run.
     *
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        MysqliMock::init();
        parent::setUpBeforeClass();
    }

    /**
     * Returns an ORM Middleware with specific parameters
     *
     * @param array  $additionalConfig Overwrite some config elements.
     * @param string $requestUri       Overwrite the request URI.
     *
     * @return ORM
     */
    public static function getMiddleware(array $additionalConfig = array(), string $requestUri = '/'): ORM
    {
        $configData = array_merge([
            'orm' => [
                'engine' => 'MySQL',
                'mysql_hostname' => 'foo.bar',
                'mysql_username' => 'foo',
                'mysql_password' => 'bar',
                'mysql_database' => 'foobar',
                'mysql_port' => 1337,
            ],
        ], $additionalConfig);

        $config = Config::__set_state([
            'data' => $configData,
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
        ]);

        $request = new Request(['REQUEST_URI' => $requestUri]);

        return new ORM($request, $config);
    }

    /**
     * Tests the getConnection() method
     *
     * @return void
     */
    public function testGetConnection(): void
    {
        $orm = static::getMiddleware();

        $this->assertInstanceOf(\mysqli::class, $orm->getConnection());
    }

    /**
     * Tests with a non-existent database engine
     *
     * @return void
     */
    public function testNonExistentEngine(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Engine Foobar not available');
        static::getMiddleware(['orm' => ['engine' => 'Foobar']]);
    }

    /**
     * Tests with a class that doesn't implement the correct engine methods
     *
     * @return void
     */
    public function testBrokenEngine(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage(__CLASS__ . ' does not implement ' . EngineInterface::class);
        static::getMiddleware(['orm' => ['engine' => __CLASS__]]);
    }
}

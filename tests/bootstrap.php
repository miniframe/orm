<?php

if (!isset($GLOBALS['skipAutoload']) || $GLOBALS['skipAutoload'] !== true) {
    require_once __DIR__ . '/../vendor/autoload.php';
}

foreach (glob(__DIR__ . '/../examples/*.php') as $exampleModel) {
    require_once $exampleModel;
}

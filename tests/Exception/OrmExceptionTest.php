<?php

namespace Miniframe\ORM\Exception;

use PHPUnit\Framework\TestCase;

class OrmExceptionTest extends TestCase
{
    /**
     * Tests constructing an ORM exception with Duplicate key as message
     *
     * @return void
     */
    public function testDuplicateKey(): void
    {
        // Build test data
        $previous = new \RuntimeException('Previous exception');
        $test = new OrmException(OrmException::DUPLICATE_KEY, null, $previous);
        $line = __LINE__ - 1;
        $file = __FILE__;

        // Assert object
        $this->assertEquals('Duplicate entry for key', $test->getMessage());
        $this->assertEquals($line, $test->getLine());
        $this->assertEquals($file, $test->getFile());
        $this->assertEquals($previous, $test->getPrevious());
        $this->assertEquals(OrmException::DUPLICATE_KEY, $test->getCode());
    }

    /**
     * Tests constructing an ORM exception with a custom message
     *
     * @return void
     */
    public function testDuplicateKeyCustomMessage(): void
    {
        // Build test data
        $previous = new \RuntimeException('Previous exception');
        $test = new OrmException(OrmException::DUPLICATE_KEY, 'This is a custom message', $previous);
        $line = __LINE__ - 1;
        $file = __FILE__;

        // Assert object
        $this->assertEquals('This is a custom message', $test->getMessage());
        $this->assertEquals($line, $test->getLine());
        $this->assertEquals($file, $test->getFile());
        $this->assertEquals($previous, $test->getPrevious());
        $this->assertEquals(OrmException::DUPLICATE_KEY, $test->getCode());
    }

    /**
     * Tests constructing an ORM exception with Duplicate key as message
     *
     * @return void
     */
    public function testUnexpected(): void
    {
        // Build test data
        $previous = new \RuntimeException('Previous exception');
        $test = new OrmException(OrmException::UNEXPECTED, null, $previous);
        $line = __LINE__ - 1;
        $file = __FILE__;

        // Assert object
        $this->assertEquals('Unexpected error', $test->getMessage());
        $this->assertEquals($line, $test->getLine());
        $this->assertEquals($file, $test->getFile());
        $this->assertEquals($previous, $test->getPrevious());
        $this->assertEquals(OrmException::UNEXPECTED, $test->getCode());
    }
}

<?php

namespace Miniframe\ORM\Repository;

use App\Model\Example;
use Garrcomm\PHPUnitHelpers\FunctionMock;
use Garrcomm\PHPUnitHelpers\Mysqli\MysqliMock;
use Miniframe\ORM\Middleware\ORMTest;
use PHPUnit\Framework\TestCase;

class GenericRepositoryTest extends TestCase
{
    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        MysqliMock::reset();
        FunctionMock::releaseAll();
        parent::tearDown();
    }

    /**
     * This method is called before the first test of this test class is run.
     *
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        MysqliMock::init();
        parent::setUpBeforeClass();
    }

    /**
     * Tests constructing a repository for a non-existing table.
     *
     * @return void
     */
    public function testConstructorWhenTableDoesntExist(): void
    {
        $tableCreated = false;
        $noticeGiven = false;

        // Respond to a SHOW COLUMNS query with a table not exist exception,
        // and to a CREATE TABLE query with success.
        MysqliMock::setQueryResponder(
            function (string $query, int $result_mode = MYSQLI_STORE_RESULT) use (&$tableCreated) {
                $showQuery = 'SHOW COLUMNS FROM `example`';
                $createQuery = 'CREATE TABLE `example` ( `id` int UNSIGNED NOT NULL AUTO_INCREMENT ,`creation_date` '
                    . 'datetime NOT NULL ,`name` varchar(255) NOT NULL ,`full_address` text NOT NULL ,`tag` '
                    . 'varchar(255) DEFAULT NULL ,`camel_cased_text` varchar(255) NOT NULL ,PRIMARY KEY (`id`))';

                if ($query == $showQuery && $result_mode == MYSQLI_STORE_RESULT) {
                    // Simulate as if the table doesn't exist
                    throw new \mysqli_sql_exception('Table \'dbname.example\' doesn\'t exist', 1146);
                } elseif ($query == $createQuery && $result_mode == MYSQLI_STORE_RESULT) {
                    // Record the table as created
                    $tableCreated = true;
                    return true;
                }
                return null;
            }
        );

        // Expect a specific user notice
        FunctionMock::mock(
            'Miniframe\\ORM\\Engine',
            'trigger_error',
            function (string $message, int $error_level = E_USER_NOTICE) use (&$noticeGiven) {
                if ($message == "Table example didn't exist, created" && $error_level == E_USER_NOTICE) {
                    $noticeGiven = true;
                    return;
                }
                throw new \ErrorException($message, $error_level);
            }
        );

        // Construct the repository
        ORMTest::getMiddleware()->getRepository(Example::class);

        // Was the table created and the notice given?
        $this->assertTrue($tableCreated);
        $this->assertTrue($noticeGiven);
    }

    /**
     * Constructs a generic repository based on the Example model. Also asserts the constructor.
     *
     * @return GenericRepository
     */
    protected function getExampleRepository(): GenericRepository
    {
        $structureRequested = false;

        // Respond to a SHOW COLUMNS query with a table not exist exception,
        // and to a CREATE TABLE query with success.
        MysqliMock::setQueryResponder(
            function (string $query, int $result_mode = MYSQLI_STORE_RESULT) use (&$structureRequested) {
                $showQuery = 'SHOW COLUMNS FROM `example`';

                if ($query == $showQuery && $result_mode == MYSQLI_STORE_RESULT) {
                    $structureRequested = true;
                    return \mysqli_result::mockFromArray($this->getShowColumnsFromExampleData());
                }
                return null;
            }
        );

        // Construct the repository
        $return = ORMTest::getMiddleware()->getRepository(Example::class);

        // Was the table structure requested?
        $this->assertTrue($structureRequested);

        return $return;
    }

    /**
     * Tests getNew()
     *
     * @return void
     */
    public function testGetNew(): void
    {
        $repository = $this->getExampleRepository();
        $this->assertInstanceOf(Example::class, $repository->getNew());
    }

    /**
     * Tests find()
     *
     * @return void
     */
    public function testFind(): void
    {
        $repository = $this->getExampleRepository();

        MysqliMock::setQueryResponder(
            function (string $query, int $result_mode = MYSQLI_STORE_RESULT) {
                $selectQuery = "SELECT `id`, `creation_date`, `name`, `full_address`, `tag`, `camel_cased_text` FROM "
                    . "`example` WHERE `id` = 1 LIMIT 1";
                if ($query == $selectQuery && $result_mode == MYSQLI_STORE_RESULT) {
                    return \mysqli_result::mockFromArray([$this->getSelectFromExampleData()[0]]);
                }
                return null;
            }
        );

        // Find one row and assert its values
        $result = $repository->find(1);
        $this->assertInstanceOf(Example::class, $result);
        $this->assertEquals(1, $result->getId());
        $this->assertInstanceOf(\DateTime::class, $result->getCreationDate());
        $this->assertEquals('1983-08-11 16:00:00', $result->getCreationDate()->format('Y-m-d H:i:s'));
        $this->assertEquals('Foo bar', $result->getName());
        $this->assertEquals('Foobar 123', $result->getAddress());
        $this->assertEquals('Baz', $result->getTag());
        $this->assertEquals('notCamelCased', $result->getCamelCasedText());
    }

    /**
     * Dataprovider for the findAll() call with all forms of input
     *
     * @return array[]
     */
    public function findAllDataProvider(): array
    {
        $baseQuery = "SELECT `id`, `creation_date`, `name`, `full_address`, `tag`, `camel_cased_text` FROM `example`";
        return [
            ['start' => 0,  'limit' => null, 'expectedQuery' => $baseQuery],
            ['start' => 10, 'limit' => null, 'expectedQuery' => $baseQuery . " OFFSET 10"],
            ['start' => 10, 'limit' => 10,   'expectedQuery' => $baseQuery . " LIMIT 10, 10"],
            ['start' => 0,  'limit' => 10,   'expectedQuery' => $baseQuery . " LIMIT 10"],
        ];
    }

    /**
     * Tests findAll()
     *
     * @param integer      $start         Start value.
     * @param integer|null $limit         Limit value.
     * @param string       $expectedQuery The expected MySQL query.
     *
     * @dataProvider findAllDataProvider
     * @return       void
     */
    public function testFindAll(int $start, ?int $limit, string $expectedQuery): void
    {
        $repository = $this->getExampleRepository();

        MysqliMock::setQueryResponder(
            function (string $query, int $result_mode = MYSQLI_STORE_RESULT) use ($expectedQuery) {
                if (
                    trim(preg_replace('/[\s]+/', ' ', $query)) ==
                    trim(preg_replace('/[\s]+/', ' ', $expectedQuery))
                    && $result_mode == MYSQLI_STORE_RESULT
                ) {
                    return \mysqli_result::mockFromArray($this->getSelectFromExampleData());
                }
                return null;
            }
        );

        // Find rows and assert return data
        $result = $repository->findAll($start, $limit);
        $this->assertIsArray($result);

        $this->assertInstanceOf(Example::class, $result[0]);
        $this->assertEquals(1, $result[0]->getId());
        $this->assertInstanceOf(\DateTime::class, $result[0]->getCreationDate());
        $this->assertEquals('1983-08-11 16:00:00', $result[0]->getCreationDate()->format('Y-m-d H:i:s'));
        $this->assertEquals('Foo bar', $result[0]->getName());
        $this->assertEquals('Foobar 123', $result[0]->getAddress());
        $this->assertEquals('Baz', $result[0]->getTag());
        $this->assertEquals('notCamelCased', $result[0]->getCamelCasedText());

        $this->assertInstanceOf(Example::class, $result[1]);
        $this->assertEquals(2, $result[1]->getId());
        $this->assertInstanceOf(\DateTime::class, $result[1]->getCreationDate());
        $this->assertEquals('2010-02-04 18:00:00', $result[1]->getCreationDate()->format('Y-m-d H:i:s'));
        $this->assertEquals('Barfoo', $result[1]->getName());
        $this->assertEquals('Bazbar 123', $result[1]->getAddress());
        $this->assertEquals('Foo', $result[1]->getTag());
        $this->assertEquals('Testcase', $result[1]->getCamelCasedText());
    }

    /**
     * Correct mock data that is returned for SHOW COLUMNS FROM `example`
     *
     * @return array[]
     */
    private function getShowColumnsFromExampleData(): array
    {
        $fields = ['Field',            'Type',             'Null', 'Key', 'Default', 'Extra'];
        $rows = [
                  ['id',               'int(10) unsigned', 'NO',   'Pri', null,      'auto_increment'],
                  ['creation_date',    'datetime',         'NO',   '',    null,      ''],
                  ['name',             'varchar(255)',     'NO',   '',    null,      ''],
                  ['full_address',     'text',             'NO',   '',    null,      ''],
                  ['tag',              'varchar(255)',     'YES',  '',    null,      ''],
                  ['camel_cased_text', 'varchar(255)',     'NO',   '',    null,      ''],
        ];

        array_walk($rows, function (array &$value) use ($fields) {
            $value = array_combine($fields, $value);
        });

        return $rows;
    }

    /**
     * Correct mock data that is returned for SELECT FROM `example`
     *
     * @return array[]
     */
    public function getSelectFromExampleData(): array
    {
        return [
            [
                'id'               => 1,
                'creation_date'    => '1983-08-11 16:00:00',
                'name'             => 'Foo bar',
                'full_address'     => 'Foobar 123',
                'tag'              => 'Baz',
                'camel_cased_text' => 'notCamelCased'
            ],
            [
                'id'               => 2,
                'creation_date'    => '2010-02-04 18:00:00',
                'name'             => 'Barfoo',
                'full_address'     => 'Bazbar 123',
                'tag'              => 'Foo',
                'camel_cased_text' => 'Testcase'
            ],
        ];
    }
}

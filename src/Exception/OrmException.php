<?php

namespace Miniframe\ORM\Exception;

class OrmException extends \Exception
{
    public const
        UNEXPECTED = 0,
        DUPLICATE_KEY = 1
    ;

    /**
     * ORM Exception
     *
     * @param integer         $code     One of the OrmException:: constants.
     * @param null|string     $message  The Exception message. When null, a default based on the code will be used.
     * @param null|\Throwable $previous The previous throwable used for the exception chaining.
     */
    public function __construct(int $code = self::UNEXPECTED, string $message = null, \Throwable $previous = null)
    {
        parent::__construct($message ?? $this->codeToMessage($code), $code, $previous);
    }

    /**
     * Converts a code to a message.
     *
     * @param integer $code The code.
     *
     * @return string
     */
    private function codeToMessage(int $code): string
    {
        switch ($code) {
            case static::DUPLICATE_KEY:
                return 'Duplicate entry for key';
            case static::UNEXPECTED:
            default:
                return 'Unexpected error';
        }
    }
}

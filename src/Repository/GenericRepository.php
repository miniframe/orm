<?php

namespace Miniframe\ORM\Repository;

use Miniframe\Annotation\Service\AnnotationReader;
use Miniframe\ORM\Annotation\Column;
use Miniframe\ORM\Annotation\OneToMany;
use Miniframe\ORM\Annotation\Table;
use Miniframe\ORM\Engine\EngineInterface;
use Miniframe\ORM\Exception\OrmException;
use Miniframe\ORM\Collection\OneToMany as OneToManyCollection;

class GenericRepository
{
    /**
     * Name of the database table
     *
     * @var Table
     */
    protected $table;

    /**
     * All table columns
     *
     * @var Column[]
     */
    protected $tableColumns = array();

    /**
     * All primary key columns
     *
     * @var Column[]
     */
    protected $primaryKey = array();

    /**
     * All one to many relationships
     *
     * @var OneToMany[]
     */
    protected $oneToMany = array();

    /**
     * The auto increment column, when it exists
     *
     * @var Column|null
     */
    protected $autoIncrement;

    /**
     * Fully qualified classname for the entity objects
     *
     * @var string
     */
    protected $entityClassName;

    /**
     * Reference to the database engine
     *
     * @var EngineInterface
     */
    protected $engine;

    /**
     * Reference to the reflection class for the current entity
     *
     * @var \ReflectionClass
     */
    protected $reflectionClass;

    /**
     * Initializes a new data repository
     *
     * @param EngineInterface $engine          Reference to the database engine.
     * @param string          $entityClassName Fully qualified classname for the related entity object.
     */
    public function __construct(EngineInterface $engine, string $entityClassName)
    {
        $this->entityClassName = $entityClassName;
        $this->engine = $engine;
        $this->reflectionClass = new \ReflectionClass($entityClassName);

        // Validate entity class
        $tableData = (new AnnotationReader())->getClass($this->reflectionClass);
        if (!$tableData->getAnnotation(Table::class)) {
            throw new \RuntimeException($entityClassName . ' doesn\'t have a @Table annotation');
        }

        // Determine table name
        $this->table = $tableData->getAnnotation(Table::class);
        if (!$this->table->name) {
            $this->table->name = $this->toSnakeCase($tableData->getShortClassName());
        }

        $properties = $tableData->getProperties();
        foreach ($properties as $propertyName => $propertyData) {
            // Determine one to many joins
            $oneToMany = $propertyData->getAnnotation(OneToMany::class);
            if ($oneToMany) {
                if ($oneToMany->localJoinProperty === null && in_array('id', array_keys($properties))) {
                    $oneToMany->localJoinProperty = 'id';
                } elseif ($oneToMany->localJoinProperty === null) {
                    throw new \RuntimeException('Please configure a localJoinProperty for a oneToMany join');
                }
                if ($oneToMany->remoteJoinProperty === null) {
                    $oneToMany->remoteJoinProperty = $entityClassName;
                }
                if (!class_exists($oneToMany->remoteEntityClass)) {
                    $ns = substr($entityClassName, 0, strrpos($entityClassName, '\\'));
                    $oneToMany->remoteEntityClass = $ns . '\\' . $oneToMany->remoteEntityClass;
                }
                if (!class_exists($oneToMany->remoteEntityClass)) {
                    throw new \RuntimeException('Cannot resolve remoteEntityClass: ' . $oneToMany->remoteEntityClass);
                }
                $this->oneToMany[$propertyName] = $oneToMany;
            }

            // Determine columns
            $columnData = $propertyData->getAnnotation(Column::class);
            if (!$columnData) {
                continue;
            }
            // Populate column data
            if (!$columnData->name) {
                $columnData->name = $this->toSnakeCase($propertyData->getName());
            }
            if (!$columnData->type && $propertyData->getAnnotation('var')) {
                switch (array_reverse(explode('|', $propertyData->getAnnotation('var')->value))[0]) {
                    case 'int':
                        $columnData->type = 'int';
                        break;
                    case 'bool':
                    case 'boolean':
                        $columnData->type = 'int';
                        $columnData->length = 1;
                        break;
                    case '\DateTime':
                        $columnData->type = 'datetime';
                        break;
                    default:
                        $columnData->type = 'varchar';
                }
            } elseif (!$columnData->type) {
                $columnData->type = 'varchar';
            }
            if (!$columnData->length && $columnData->type == 'varchar') {
                $columnData->length = 255;
            }
            $this->tableColumns[$propertyName] = $columnData;
            if ($columnData->primaryKey) {
                $this->primaryKey[$propertyName] = $columnData;
            }
            if ($columnData->autoIncrement && $this->autoIncrement) {
                throw new \RuntimeException($entityClassName . ' has more then 1 auto increment values');
            } elseif ($columnData->autoIncrement) {
                $this->autoIncrement = $columnData;
            }
        }

        // Default primary key to 'id' when possible
        if (count($this->primaryKey) == null && isset($this->tableColumns['id'])) {
            $this->tableColumns['id']->primaryKey = true;
            $this->primaryKey[] = $this->tableColumns['id'];
        }

        // Table requirements
        if (count($this->tableColumns) === 0) {
            throw new \RuntimeException('No @Column annotations found in ' . $entityClassName);
        }
        if (count($this->primaryKey) === 0) {
            throw new \RuntimeException('No primary key defined in ' . $entityClassName);
        }

        // Prepare table
        $engine->prepareTable($this->table, $this->tableColumns);
    }

    /**
     * Returns a new, empty, model
     *
     * @return object
     */
    public function getNew()
    {
        return new $this->entityClassName();
    }

    /**
     * Returns a single entity matching the primary key to a specific value.
     *
     * Only works when there's a single column defined as primary key.
     *
     * @param integer|string $identifier The value of the primary key.
     *
     * @return object|null
     */
    public function find($identifier)
    {
        if (count($this->primaryKey) !== 1) {
            throw new \RuntimeException('find() only works when there\'s one primary key column');
        }
        return $this->findOneBy([array_key_first($this->primaryKey) => $identifier]);
    }

    /**
     * Returns a single entity matching a set of properties.
     *
     * @param array $properties The list of properties.
     *
     * @return object|null
     */
    public function findOneBy(array $properties)
    {
        $result = $this->findBy($properties, 0, 1);
        return $result[0] ?? null;
    }

    /**
     * Returns a set of all entities
     *
     * @param integer      $start The offset.
     * @param integer|null $limit The max. amount of entities to return.
     *
     * @return object[]
     */
    public function findAll(int $start = 0, ?int $limit = null): array
    {
        return $this->findBy([], $start, $limit);
    }

    /**
     * Returns a set of entities matching specific properties
     *
     * @param array        $properties The list of properties.
     * @param integer      $start      The offset.
     * @param integer|null $limit      The max. amount of entities to return.
     *
     * @return object[]
     */
    public function findBy(array $properties, int $start = 0, ?int $limit = null): array
    {
        $parameters = array();
        $query = 'SELECT ';
        foreach ($this->tableColumns as $propertyName => $column) {
            $query .= '`' . $column->name . '`, ';
        }
        $query = substr($query, 0, -2) . ' FROM `' . $this->table->name . '` WHERE';
        foreach ($properties as $property => $value) {
            $column = $this->tableColumns[$property];
            $query .= ' `' . $column->name . '` = :' . $column->name . ' AND ';
            $parameters[$column->name] = $value;
        }
        $query = substr($query, 0, -5); // Removes 'WHERE' or ' AND ', depending if there are properties

        if ($start > 0 && $limit === null) {
            $query .= ' OFFSET ' . $start;
        } elseif ($start === 0 && $limit !== null) {
            $query .= ' LIMIT ' . $limit;
        } elseif ($start > 0 && $limit !== null) {
            $query .= ' LIMIT ' . $start . ', ' . $limit;
        }

        $result = $this->engine->fetch($query, $parameters);

        // Create dummies for the oneToMany relations
        $return = array();
        foreach ($result as $entityData) {
            $return[] = $this->convertArrayToEntity($entityData);
        }

        return $return;
    }

    /**
     * Converts an associative array to a model class
     *
     * @param array $entityData The associative array.
     *
     * @return mixed
     */
    protected function convertArrayToEntity(array $entityData)
    {
        $entity = new $this->entityClassName();
        foreach ($this->tableColumns as $propertyName => $columnData) {
            if ($columnData->type == 'datetime') {
                $entityData[$columnData->name] = new \DateTime($entityData[$columnData->name]);
            } elseif (in_array($columnData->type, ['bool', 'boolean'])) {
                $entityData[$columnData->name] = $entityData[$columnData->name] ?? false;
            }
            $this->setPropertyToEntity($entity, $propertyName, $entityData[$columnData->name]);
        }
        foreach ($this->oneToMany as $propertyName => $oneToMany) {
            $this->setPropertyToEntity($entity, $propertyName, new OneToManyCollection(
                $oneToMany->remoteEntityClass,
                $oneToMany->remoteJoinProperty,
                $this->getPropertyFromEntity($entity, $oneToMany->localJoinProperty)
            ));
        }
        return $entity;
    }

    /**
     * Removes an entity
     *
     * @param object $object The entity.
     *
     * @return void
     */
    public function delete(object $object): void
    {
        $properties = $this->getPropertiesFromEntity($object);

        $parameters = array();
        $query = 'DELETE FROM `' . $this->table->name . '` WHERE ';
        foreach ($this->primaryKey as $column) {
            $query .= '`' . $column->name . '` = :' . $column->name . ' AND ';
            $parameters[$column->name] = $properties[$column->name];
        }
        $query = substr($query, 0, -4) . ' LIMIT 1;';

        $this->engine->query($query, $parameters);
    }

    /**
     * Saves an entity
     *
     * @param object $object The entity.
     *
     * @return void
     */
    public function save(object $object): void
    {
        if (!is_a($object, $this->entityClassName)) {
            throw new \RuntimeException('Invalid repository for ' . get_class($object));
        }

        $this->executeSave($object);
    }

    /**
     * Executes a save call
     *
     * @param object  $object The object.
     * @param boolean $insert True for INSERT, false for UPDATE.
     *
     * @return void
     */
    protected function executeSave(object $object, bool $insert = false): void
    {
        $properties = $this->getPropertiesFromEntity($object);

        if ($insert) {
            $query = 'INSERT INTO `' . $this->table->name . '` SET ';
        } else {
            $query = "UPDATE `" . $this->table->name . '`SET ';
        }
        $parameters = array();
        foreach ($properties as $propertyName => $propertyValue) {
            if (
                isset($this->autoIncrement->name)
                && $propertyName === $this->autoIncrement->name
                && $propertyValue === null
            ) {
                continue;
            }
            $query .= '`' . $propertyName . '` = :' . $propertyName . ', ';
            $parameters[$propertyName] = $propertyValue;
        }
        $query = substr($query, 0, -2);

        if (!$insert) {
            $query .= ' WHERE ';
            foreach ($this->primaryKey as $primaryKey) {
                $query .= '`' . $primaryKey->name . '` = :' . $primaryKey->name . ' AND ';
                $parameters[$primaryKey->name] = $properties[$primaryKey->name];
            }
            $query = substr($query, 0, -4);
        }

        try {
            $result = $this->engine->query($query, $parameters);
        } catch (OrmException $exception) {
            // When inserting, apparently the UPDATE gave 0 results, so no changes
            if ($insert && $exception->getCode() == OrmException::DUPLICATE_KEY) {
                return;
            }
            throw $exception;
        }

        if ($insert && $this->autoIncrement !== null) {
            $this->setPropertyToEntity($object, $this->autoIncrement->name, $result);
        }
        if (!$insert && $result === 0) {
            $this->executeSave($object, true);
        }
    }

    /**
     * Fetches all properties from an object and returns an array with columnName => value
     *
     * @param object $object The object.
     *
     * @return array
     */
    protected function getPropertiesFromEntity(object $object): array
    {
        $return = array();
        foreach ($this->tableColumns as $propertyName => $columnData) {
            $return[$columnData->name] = $this->getPropertyFromEntity($object, $propertyName);
        }
        return $return;
    }

    /**
     * Fetches all properties from an object and returns an array with columnName => value
     *
     * @param object $object       The object.
     * @param string $propertyName The property name.
     *
     * @return mixed
     */
    protected function getPropertyFromEntity(object $object, string $propertyName)
    {
        $pc = $this->reflectionClass->getProperty($propertyName);
        if (!$pc->isPublic()) {
            $pc->setAccessible(true);
            $return = $pc->getValue($object);
            $pc->setAccessible(false);
        } else {
            $return = $object->$propertyName;
        }

        return $return;
    }

    /**
     * Sets a specific value in an object
     *
     * @param object $object       The object.
     * @param string $propertyName The property name.
     * @param mixed  $value        The new value.
     *
     * @return void
     */
    protected function setPropertyToEntity(object $object, string $propertyName, $value): void
    {
        $pc = $this->reflectionClass->getProperty($propertyName);
        if (!$pc->isPublic()) {
            $pc->setAccessible(true);
            $pc->setValue($object, $value);
            $pc->setAccessible(false);
        } else {
            $object->{$propertyName} = $value;
        }
    }

    /**
     * Converts PascalCase, camelCase and kebab-case to snake_case
     *
     * @param string $input The input string.
     *
     * @return string
     */
    protected function toSnakeCase(string $input): string
    {
        // Convert PascalCase to camelCase
        $input = lcfirst($input);
        // Convert kebab-case to snake_case
        $input = str_replace('-', '_', $input);
        // Convert camelCase to snake_case
        $input = strtolower(preg_replace('/(?<!^)[A-Z]/u', '_$0', $input));

        return $input;
    }
}

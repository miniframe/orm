<?php

namespace Miniframe\ORM\Annotation;

/**
 * Table annotation
 *
 * @Annotation
 */
class Table
{
    /**
     * Table name
     *
     * @var string
     */
    public $name;

    /**
     * Reference to a repository
     *
     * @var object|null
     */
    public $repository;
}

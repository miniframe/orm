<?php

namespace Miniframe\ORM\Annotation;

/**
 * ManyToOne annotation
 *
 * @Annotation
 */
class OneToMany
{
    /**
     * Fully qualified classname for the remote entity.
     *
     * @var object
     */
    public $remoteEntityClass;

    /**
     * Remote column referencing to the identifier for the current entity (default: name of the entity itself)
     *
     * @var string
     */
    public $remoteJoinProperty;

    /**
     * Identifier property of the current entity used for the join (default: the primary key column)
     *
     * @var string
     */
    public $localJoinProperty;
}

<?php

namespace Miniframe\ORM\Annotation;

/**
 * Column annotation
 *
 * @Annotation
 */
class Column
{
    /**
     * Is the column the primary key?
     *
     * @var bool
     */
    public $primaryKey = false;

    /**
     * Is the value unsigned (only positive numbers) or signed (positive and negative numbers)?
     *
     * @var bool
     */
    public $unsigned = false;

    /**
     * Is autoincrement enabled?
     *
     * @var bool
     */
    public $autoIncrement = false;

    /**
     * Can the column contain null values?
     *
     * @var bool
     */
    public $nullable = false;

    /**
     * Column name
     *
     * @var string
     */
    public $name;

    /**
     * Data type
     *
     * @Enum({"text","int","varchar","boolean","date","datetime"})
     */
    public $type;

    /**
     * Length of the value
     *
     * @var int
     */
    public $length;

    /**
     * Default value
     *
     * @var string
     */
    public $default;

    /**
     * Name of the index (must be unique for the whole database)
     *
     * @var string
     */
    public $index;
}

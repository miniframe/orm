<?php

namespace Miniframe\ORM\Middleware;

use Miniframe\Annotation\Service\AnnotationReader;
use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\ORM\Annotation\Table;
use Miniframe\ORM\Engine\EngineInterface;
use Miniframe\ORM\Repository\GenericRepository;

class ORM extends AbstractMiddleware
{
    /**
     * Reference to the database engine
     *
     * @var EngineInterface
     */
    protected $engine;

    /**
     * List of repositories
     *
     * @var GenericRepository[]
     */
    protected $repositories = array();

    /**
     * Initializes the Miniframe ORM Middleware package
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        $engine = $config->get('orm', 'engine');
        if (!class_exists($engine) && class_exists('Miniframe\\ORM\\Engine\\' . $engine)) {
            $engine = 'Miniframe\\ORM\\Engine\\' . $engine;
        } elseif (!class_exists($engine)) {
            throw new \RuntimeException('Engine ' . $engine . ' not available');
        }
        if (!in_array(EngineInterface::class, class_implements($engine))) {
            throw new \RuntimeException($engine . ' does not implement ' . EngineInterface::class);
        }
        $this->engine = new $engine($config);
    }

    /**
     * Returns the repository for a specific entity.
     *
     * @param string $entityClassName Fully qualified classname of the entity.
     *
     * @return GenericRepository
     */
    public function getRepository(string $entityClassName): GenericRepository
    {
        if (!isset($this->repositories[$entityClassName])) {
            $repository = (new AnnotationReader())
                ->getClass(new \ReflectionClass($entityClassName))
                ->getAnnotation(Table::class)
                ->repository;
            if (!$repository) {
                $repository = GenericRepository::class;
            } else {
                if (!in_array(GenericRepository::class, class_parents($repository))) {
                    throw new \RuntimeException($repository . ' doesn\'t extend ' . GenericRepository::class);
                }
            }
            $this->repositories[$entityClassName] = new $repository($this->engine, $entityClassName);
        }
        return $this->repositories[$entityClassName];
    }

    /**
     * Returns the connection of the engine (result may differ per engine)
     *
     * @return mixed
     */
    public function getConnection()
    {
        return $this->engine->getConnection();
    }
}

<?php

namespace Miniframe\ORM\Collection;

use Miniframe\Core\Registry;
use Miniframe\ORM\Middleware\ORM;

class OneToMany implements \ArrayAccess, \Iterator, \Countable
{
    /**
     * The actual collection
     *
     * @var array|null
     */
    protected $collection = null;
    /**
     * Position within the array
     *
     * @var int|string|null
     */
    protected $position = null;

    /**
     * Name of the type of entities in this collection
     *
     * @var string
     */
    protected $entityClassName;
    /**
     * Name of the column that should match $columnValue
     *
     * @var string
     */
    protected $columnName;
    /**
     * The value that should be in $columnName
     *
     * @var mixed
     */
    protected $columnValue;

    /**
     * Collection object for a OneToMany join
     *
     * @param string $entityClassName Name of the type of entities in this collection.
     * @param string $columnName      Name of the column that should match $columnValue.
     * @param mixed  $columnValue     The value that should be in $columnName.
     */
    public function __construct(string $entityClassName, string $columnName, $columnValue)
    {
        $this->entityClassName = $entityClassName;
        $this->columnName = $columnName;
        $this->columnValue = $columnValue;
    }

    /**
     * Prepares the data collection when it's accessed for the first time
     *
     * @return void
     */
    protected function prepare(): void
    {
        if ($this->collection !== null) {
            return;
        }
        $this->collection = Registry::get(ORM::class)
            ->getRepository($this->entityClassName)
            ->findBy([$this->columnName => $this->columnValue]);

        $this->rewind();
    }

    /**
     * ArrayAccess::offsetExists — Whether an offset exists
     *
     * @param mixed $offset An offset to check for.
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        $this->prepare();
        return isset($this->collection[$offset]); // @TODO maybe use array_key_exists ?
    }

    /**
     * ArrayAccess::offsetGet — Offset to retrieve
     *
     * @param mixed $offset The offset to retrieve.
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        $this->prepare();
        return $this->collection[$offset];
    }

    /**
     * ArrayAccess::offsetSet — Assign a value to the specified offset
     *
     * @param mixed $offset The offset to assign the value to.
     * @param mixed $value  The value to set.
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        $this->prepare();
        if (is_null($offset)) {
            $this->collection[] = $value;
        } else {
            $this->collection[$offset] = $value;
        }
    }

    /**
     * ArrayAccess::offsetUnset — Unset an offset
     *
     * @param mixed $offset The offset to unset.
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        $this->prepare();
        unset($this->collection[$offset]);
    }

    /**
     * Iterator::current — Return the current element
     *
     * @return mixed
     */
    public function current()
    {
        $this->prepare();
        return $this->collection[$this->position] ?? null;
    }

    /**
     * Iterator::next — Move forward to next element
     *
     * @return void
     */
    public function next(): void
    {
        $this->prepare();
        $keys = array_keys($this->collection);
        $index = array_search($this->position, $keys) + 1;
        $this->position = $keys[$index] ?? null;
    }

    /**
     * Iterator::key — Return the key of the current element
     *
     * @return mixed
     */
    public function key()
    {
        $this->prepare();
        return $this->position;
    }

    /**
     * Iterator::valid — Checks if current position is valid
     *
     * @return boolean
     */
    public function valid(): bool
    {
        $this->prepare();
        return $this->position !== null;
    }

    /**
     * Iterator::rewind — Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind(): void
    {
        $this->prepare();
        $this->position = array_key_first($this->collection);
    }

    /**
     * Countable::count — Count elements of an object
     *
     * @return integer
     */
    public function count(): int
    {
        $this->prepare();
        return count($this->collection);
    }
}

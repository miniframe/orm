<?php

namespace Miniframe\ORM\Engine;

use Miniframe\Core\Config;
use Miniframe\ORM\Annotation\Column;
use Miniframe\ORM\Annotation\Table;
use Miniframe\ORM\Exception\OrmException;

class MySQL implements EngineInterface
{
    /**
     * Reference to the MySQL connection
     *
     * @var \mysqli
     */
    protected $connection;

    /**
     * List of config values
     *
     * @var array
     */
    private $configValues = array();

    /**
     * Initializes the MySQL engine
     *
     * @param Config $config Reference to the configuration object.
     */
    public function __construct(Config $config)
    {
        foreach (['hostname', 'username', 'password', 'database', 'port', 'socket'] as $configName) {
            $this->configValues[$configName] = $config->has('orm', 'mysql_' . $configName)
                ? $config->get('orm', 'mysql_' . $configName)
                : null;
        }
        // @codeCoverageIgnoreStart
        // This exception can't be tested since other tests won't work if \mysqli isn't available
        if (!class_exists(\mysqli::class)) {
            throw new \RuntimeException('When using the MySQL engine, the mysqli extension is required.');
        }
        // @codeCoverageIgnoreEnd

        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->connect();
    }

    /**
     * Creates the MySQL connection
     *
     * @return void
     */
    private function connect(): void
    {
        $this->connection = new \mysqli(
            $this->configValues['hostname'],
            $this->configValues['username'],
            $this->configValues['password'],
            $this->configValues['database'],
            $this->configValues['port'],
            $this->configValues['socket']
        );
    }

    /**
     * Returns the mysqli connection
     *
     * @return \mysqli
     */
    public function getConnection(): \mysqli
    {
        return $this->connection;
    }

    /**
     * Fetch column metadata from table
     *
     * @param string $tableName Name of the table.
     *
     * @return array
     *
     * @see prepareTable
     * @see https://dev.mysql.com/doc/refman/8.0/en/show-columns.html
     */
    private function getColumns(string $tableName): array
    {
        $mysqlColumns = array();
        try {
            $result = $this->connection->query('SHOW COLUMNS FROM `' . $tableName . '`');
        } catch (\mysqli_sql_exception $exception) {
            if ($exception->getMessage() == 'MySQL server has gone away') { // @TODO Use getSqlState() on exception?
                $this->connect();
                $result = $this->connection->query('SHOW COLUMNS FROM `' . $tableName . '`');
            } else {
                throw $exception;
            }
        }
        while ($mysqlColumn = $result->fetch_assoc()) {
            $mysqlColumns[$mysqlColumn['Field']] = $mysqlColumn;
        }
        return $mysqlColumns;
    }

    /**
     * Creates a table
     *
     * @param string   $tableName Name of the table.
     * @param Column[] $columns   Column metadata.
     *
     * @return void
     *
     * @see prepareTable
     */
    private function createTable(string $tableName, array $columns): void
    {
        $createTable = 'CREATE TABLE `' . $tableName . '` ( ';
        $primaryKey = array();
        $indexes = array();
        foreach ($columns as $column) {
            if ($column->index) {
                $indexes[$column->index][] = $column->name;
            }
            $createTable .= $this->columnToSql($column) . ',';
            if ($column->primaryKey) {
                $primaryKey[] = $column->name;
            }
        }
        if (count($primaryKey)) {
            $createTable .= 'PRIMARY KEY (`' . implode('`, `', $primaryKey) . '`),';
        }
        foreach ($indexes as $indexName => $indexColumns) {
            $createTable .= 'KEY `' . $indexName . '` (`' . implode('`, `', $indexColumns) . '`),';
        }
        $createTable = rtrim($createTable, ',') . ')';

        $this->connection->query($createTable);
    }

    /**
     * Column to MySQL formatter
     *
     * @param Column $column Column metadata.
     *
     * @return string
     */
    private function columnToSql(Column $column): string
    {
        return '`' . $column->name . '` ' . $column->type
            . ($column->length ? '(' . $column->length . ') ' : ' ')
            . ($column->unsigned ? 'UNSIGNED ' : '')
            . ($column->nullable ? 'DEFAULT NULL ' : 'NOT NULL ')
            . ($column->autoIncrement ? 'AUTO_INCREMENT ' : '');
    }

    /**
     * Prepares a table
     *
     * @param Table    $table   Table metadata.
     * @param Column[] $columns Column metadata.
     *
     * @return void
     */
    public function prepareTable(Table $table, array $columns): void
    {
        // Fetch columns, if table doesn't exist, create it.
        try {
            $mysqlColumns = $this->getColumns($table->name);
        } catch (\mysqli_sql_exception $exception) {
            if ($exception->getCode() != 1146) { // 1146 = Table '%s' doesn't exist
                throw $exception;
            }
            $this->createTable($table->name, $columns);
            trigger_error('Table ' . $table->name . ' didn\'t exist, created', E_USER_NOTICE);
            return;
        }

        // Do all columns in entity match database?
        foreach ($columns as $entityColumn) {
            $dbColumnName = $entityColumn->name;
            if (!isset($mysqlColumns[$dbColumnName])) {
                $this->connection->query(
                    "ALTER TABLE `" . $table->name . "` ADD COLUMN " . $this->columnToSql($entityColumn)
                );
                trigger_error('Column ' . $table->name . '.' . $dbColumnName . ' didn\'t exist, added', E_USER_NOTICE);
                continue;
            }
            $mysqlColumn = $mysqlColumns[$dbColumnName];
            $mysqlColumns[$dbColumnName]['matched'] = true;

            // Parses the type column
            preg_match(
                '/^(?P<type>[\w]+)[\s]*(\((?P<params>.*?)\)|)(?P<extra>.*?)$/i',
                $mysqlColumn['Type'],
                $typeMatches
            );

            $correct = true;
            // Matches the type
            if (
                ($typeMatches['type'] != $entityColumn->type)
                || ($entityColumn->length && $typeMatches['params'] != $entityColumn->length)
            ) {
                $correct = false;
            }
            // Matches unsigned
            if (
                (stripos($typeMatches['extra'], 'unsigned') === false && $entityColumn->unsigned)
                || (stripos($typeMatches['extra'], 'unsigned') !== false && !$entityColumn->unsigned)
            ) {
                $correct = false;
            }
            // Matches the nullable value
            if (
                ($mysqlColumn['Null'] == 'NO' && $entityColumn->nullable)
                || ($mysqlColumn['Null'] != 'NO' && !$entityColumn->nullable)
            ) {
                $correct = false;
            }
            // Matches the primary key
            if (
                ($mysqlColumn['Key'] == 'PRI' && !$entityColumn->primaryKey)
                || ($mysqlColumn['Key'] != 'PRI' && $entityColumn->primaryKey)
            ) {
                // @TODO Handle this correctly
                //$correct = false;
            }
            // Matches auto increment
            if (
                (stripos($mysqlColumn['Extra'], 'auto_increment') === false && $entityColumn->autoIncrement)
                || (stripos($mysqlColumn['Extra'], 'auto_increment') !== false && !$entityColumn->autoIncrement)
            ) {
                $correct = false;
            }

            if (!$correct) {
                $this->connection->query(
                    "ALTER TABLE `" . $table->name . "` CHANGE `" . $dbColumnName . "` "
                    . $this->columnToSql($entityColumn)
                );
                trigger_error(
                    'Column ' . $table->name . '.' . $dbColumnName . ' didn\'t match, modified',
                    E_USER_NOTICE
                );
            }
        }

        // Are there database columns missing in the entity?
        foreach ($mysqlColumns as $mysqlColumn) {
            if (!isset($mysqlColumn['matched']) || $mysqlColumn['matched'] !== true) {
                $this->connection->query(
                    "ALTER TABLE `" . $table->name . "` DROP COLUMN `" . $mysqlColumn['Field'] . "`"
                );
                trigger_error(
                    'Column ' . $table->name . '.' . $mysqlColumn['Field'] . ' didn\'t exist in entity, removed',
                    E_USER_NOTICE
                );
            }
        }
    }

    /**
     * Executes a query
     *
     * Returns the inserted ID for INSERT queries (int or string)
     * Returns the amount of affected rows for UPDATE and DELETE queries (int)
     *
     * @param string $query      The query.
     * @param array  $parameters A list of parameters.
     *
     * @return integer|string
     */
    public function query(string $query, array $parameters = array())
    {
        if (!preg_match('/^(INSERT|UPDATE|DELETE)\s/i', $query)) {
            throw new \RuntimeException('Only use query() for INSERT, UPDATE or DELETE queries');
        }

        // For INSERT queries, return the insert_id
        if (preg_match('/^INSERT\s/i', $query)) {
            try {
                $statement = $this->getResult($query, $parameters);
            } catch (\mysqli_sql_exception $exception) {
                if ($exception->getCode() == 1062) { // 1062 = Duplicate entry '%s' for key '%s'
                    throw new OrmException(OrmException::DUPLICATE_KEY, $exception->getMessage(), $exception);
                }
                throw $exception;
            }
            $return = $statement->insert_id;
            $statement->free_result();
            return $return;
        }

        $statement = $this->getResult($query, $parameters);
        $return = $statement->affected_rows;
        $statement->free_result();
        return $return;
    }

    /**
     * Formats a parameter
     *
     * @param mixed $param The parameter.
     *
     * @return mixed
     */
    protected function formatParameter($param)
    {
        if (is_object($param) && in_array(\DateTimeInterface::class, class_implements($param))) {
            return $param->format('Y-m-d H:i:s');
        }
        return $param;
    }

    /**
     * Returns a list of character types to be injected as types for bind_param
     *
     * @param array $params The parameters.
     *
     * @return string
     */
    protected function getTypes(array $params): string
    {
        $return = '';
        foreach ($params as $param) {
            if (is_int($param) || is_bool($param)) {
                $return .= 'i';
            } elseif (is_numeric($param)) {
                $return .= 'd';
            } else {
                $return .= 's';
            }
        }
        return $return;
    }

    /**
     * Executes a query and returns an array with objects
     *
     * @param string $query      The query.
     * @param array  $parameters A list of parameters.
     *
     * @return array[]
     */
    public function fetch(string $query, array $parameters = array()): array
    {
        if (!preg_match('/^(SELECT|SHOW|DESCRIBE|EXPLAIN)\s/i', $query)) {
            throw new \RuntimeException('Only use fetch() for SELECT, SHOW, DESCRIBE and EXPLAIN queries');
        }

        $return = array();
        $result = $this->getResult($query, $parameters)->get_result();
        while ($row = $result->fetch_assoc()) {
            $return[] = $row;
        };
        $result->free();

        return $return;
    }

    /**
     * Executes a query
     *
     * @param string $query      The query.
     * @param array  $parameters A list of parameters.
     *
     * @return \mysqli_stmt
     */
    protected function getResult(string $query, array $parameters)
    {
        // Prepared query
        $newParametersList = array();
        $replacements = array();
        preg_match_all('/\:([\w]+)/', $query, $matches, PREG_SET_ORDER);
        foreach ($matches as $parameterIdentifier) {
            if (!array_key_exists($parameterIdentifier[1], $parameters)) {
                continue;
            }
            $replacements[] = $parameterIdentifier[0];
            $newParametersList[] = $this->formatParameter($parameters[$parameterIdentifier[1]]);
        }

        // Replace ":foo" values to "?", but sorted by string length. This is done because if you first replace ":foo"
        // to "?", and after that ":foobar" to "?", it won't work; it will be ":?bar" instead.
        usort($replacements, function ($a, $b) {
            return strlen($b) - strlen($a);
        });

        foreach ($replacements as $replacement) {
            $query = str_replace($replacement, '?', $query);
        }

        try {
            $statement = $this->connection->prepare($query);
        } catch (\mysqli_sql_exception $exception) {
            if ($exception->getMessage() == 'MySQL server has gone away') { // @TODO Use getSqlState() on exception?
                $this->connect();
                $statement = $this->connection->prepare($query);
            } else {
                throw $exception;
            }
        }
        if (count($newParametersList)) {
            $statement->bind_param($this->getTypes($newParametersList), ...$newParametersList);
        }
        $statement->execute();

        return $statement;
    }
}

<?php

namespace Miniframe\ORM\Engine;

use Miniframe\Core\Config;
use Miniframe\ORM\Annotation\Column;
use Miniframe\ORM\Annotation\Table;
use Miniframe\ORM\Repository\GenericRepository;

interface EngineInterface
{
    /**
     * Initializes a database engine
     *
     * @param Config $config Reference to the configuration object.
     */
    public function __construct(Config $config);

    /**
     * Returns the connection of the engine (result may differ per engine)
     *
     * @return mixed
     */
    public function getConnection();

    /**
     * Prepares a table
     *
     * @param Table    $table   Table metadata.
     * @param Column[] $columns Column metadata.
     *
     * @return void
     */
    public function prepareTable(Table $table, array $columns): void;

    /**
     * Executes a query
     *
     * Returns the inserted ID for INSERT queries (int or string)
     * Returns the amount of affected rows for UPDATE and DELETE queries (int)
     * Returns a success boolean for all other queries (bool)
     *
     * @param string $query      The query.
     * @param array  $parameters A list of parameters.
     *
     * @return integer|string|boolean
     */
    public function query(string $query, array $parameters = array());

    /**
     * Executes a query and returns an array with objects
     *
     * @param string $query      The query.
     * @param array  $parameters A list of parameters.
     *
     * @return array[]
     */
    public function fetch(string $query, array $parameters = array()): array;
}

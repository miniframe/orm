@echo off
rem This is a Docker shortcut for running unit tests on this project

rem The -n parameter disables extensions like mysqli, so we can mock that extension.
docker.exe run --rm --interactive --tty --volume "%~dp0\..:/var/www/html" garrcomm/php-apache-composer php -n -d zend_extension=xdebug -d xdebug.mode=coverage vendor/bin/phpunit %*
if "%ERRORLEVEL%" == "9009" (
    echo Docker for Windows is not installed.
    goto :eof
)
if "%ERRORLEVEL%" == "127" (
    echo Could not execute command. Have you ran "composer install" first?
    goto :eof
)

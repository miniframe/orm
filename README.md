# Miniframe ORM

This package adds a simple ORM to the [Miniframe PHP Framework](https://miniframe.dev/).

## How to configure

First, add the middleware to the framework:

```ini
[framework]
middleware[] = Miniframe\ORM\Middleware\ORM
```

Then, add an engine, for MySQL that would mean:

```ini
[orm]
engine = MySQL
mysql_username = miniframe
mysql_password = miniframepw
mysql_database = miniframedb
mysql_hostname = localhost
; mysql_port = 3306
; mysql_socket = /var/run/mysqld/mysqld.sock
```

## How to use

First define entities. An example can be found here: [Example.php](examples/Example.php)

Now, you can use this entity in your code:

```php
use App\Model\Example;
use Miniframe\Core\Registry;
use Miniframe\ORM\Middleware\ORM;

/* @var $entity Example */
/* @var $entities Example[] */

// Create a repository for the Example entity
$repository = Registry::get(ORM::class)->getRepository(Example::class);

// To get all entities
$entities = $repository->findAll();

// To get an entity by it's unique identifier
$entity = $repository->find(1);

// To get an entity by a specific column
$entity = $repository->findOneBy(['name' => 'Foo bar']);

// To get multiple entities by a specific column
$entities = $repository->findBy(['tag' => 'Foo']);

// To modify and save an entity to the database
$entity->setName('Foo bar');
$entity->setTag('foo');
$repository->save($entity);

// To delete an entity from the database
$repository->delete($entity);
```

## No flush() nor persist(), a warning!

This ORM is built to be simple, and consume little memory.
Therefor there's no central registry or cache of entities.

Why is this relevant? This explains it's relevance code-wise:

```php
use App\Model\Example;
use Miniframe\Core\Registry;
use Miniframe\ORM\Middleware\ORM;

/* @var $entity1 Example */
/* @var $entity2 Example[] */

// Create a repository for the Example entity
$repository = Registry::get(ORM::class)->getRepository(Example::class);

$entity1 = $repository->find(1);
$entity2 = $repository->find(1);

if ($entity1 == $entity2) {
    echo 'They are the same at this point' . PHP_EOL;
}

$entity1->setName('foo bar');
if ($entity1 == $entity2) {
    echo 'But they are not the same anymore' . PHP_EOL;
}
```

There are ORMs in which the 2nd `if()` statement would still match since it would be a reference to the same entity.
In this ORM, that's not the case.

## For Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/orm/addon/pipelines/home) to automatically test this project.
